/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import pt.ipp.isep.dei.esoft.gpsd.model.Categoria;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.Horario;
import pt.ipp.isep.dei.esoft.gpsd.model.PedidoServico;
import pt.ipp.isep.dei.esoft.gpsd.model.Servico;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class EfetuarPedidoServicoController {
    private Empresa m_oEmpresa;
    private Set<Servico> m_lstServicos;
    private Set<PedidoServico> m_lstPedidos;
    
    public List<Categoria> getCategorias() {
        return this.m_oEmpresa.getCategorias();
    }
    
    public List<Servico> getServicos(Categoria categoria) {
        List<Servico> lista = new ArrayList<Servico>();
        for (Servico serv:m_lstServicos) {
            if (serv.getCategoria().equals(categoria)) {
                lista.add(serv);
            }
        }
        return lista;
    }
    
    public Horario newHorario(int hora, int minuto, int segundo) {
        return new Horario(hora, minuto, segundo);
    }
    
    public PedidoServico newPedidoServico(Horario horario, String desc, int tempo) throws Exception{
        try {
            return new PedidoServico(horario, desc, tempo);
        } catch(Exception e) {
            throw e;
        }
    }   
    
    public boolean validaPedidoServico(PedidoServico pedido) {
        //Ainda não sabemos o critério de validação
        return true;
    }
    
    public void addPedidoServico(PedidoServico pedido) {
        m_lstPedidos.add(pedido);
    }
    
    public void registaPedidoServico(PedidoServico pedido) throws Exception{
        if (validaPedidoServico(pedido)) {
            addPedidoServico(pedido);
        } else {
            throw new Exception("Erro no registo de pedido de serviço.");
        }
    }
}
