/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.Objects;

/**
 *
 * @author paulomaio
 */
public class EnderecoPostal
{
    private String m_strLocal;
    private CodPostal codPostal;
    private String m_strLocalidade;
    
            
    
    public EnderecoPostal(String strLocal, CodPostal codPostal, String strLocalidade)
    {
        if ( (strLocal == null) || (codPostal == null) || (strLocalidade == null) ||
                (strLocal.isEmpty())|| (strLocalidade.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strLocal = m_strLocal;
        this.codPostal = codPostal;
        this.m_strLocalidade = m_strLocalidade;
    }
    
    public String getLocal() {
        return m_strLocal;
    }
    
    public CodPostal getCodPostal() {
        return codPostal;
    }
    
    public String getLocalidade() {
        return m_strLocalidade;
    } 
   
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hash(this.m_strLocal,this.codPostal, this.m_strLocalidade);
        return hash;
    }
    
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        EnderecoPostal obj = (EnderecoPostal) o;
        return (Objects.equals(m_strLocal, obj.m_strLocal) && 
                Objects.equals(codPostal, obj.codPostal) &&
                Objects.equals(m_strLocalidade, obj.m_strLocalidade));
    }
    
    @Override
    public String toString()
    {
        return String.format("%s \n %s - %s", this.m_strLocal, this.codPostal.toString(), this.m_strLocalidade);
    }
    
}
