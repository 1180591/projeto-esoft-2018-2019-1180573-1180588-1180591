/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.List;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class RegistoCategorias {
    private Empresa empresa;
    
    public RegistoCategorias(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public List<Categoria> getCategorias() {
        return empresa.getCategorias();
    }
    
    public Categoria getCategoriaById(String codigo) {
        for (Categoria c : empresa.getCategorias()) {
            if (c.getCodigo().equals(codigo)) {
                return c;
            }
        }
        return null;
    }
}
