/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.model.AreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.model.CodPostal;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class EspecificarAreaGeograficaController {
    List<AreaGeografica> m_lstAreas;
    
    public AreaGeografica newAreaGeografica(String nome, double custoDesloc, double raio, CodPostal codBase) throws Exception{
        try {
            return new AreaGeografica(nome, custoDesloc, raio, codBase);
        } catch(Exception e) {
            throw e;
        }
    }
    
    public boolean validaAreaGeografica(AreaGeografica area) {
        //Ainda não sabemos o critério de validação
        return true;
    }
    
    public void addAreaGeografica(AreaGeografica area) {
        m_lstAreas.add(area);
    }
    
    public void registaAreaGeografica(AreaGeografica area) throws Exception{
        if (validaAreaGeografica(area)) {
            addAreaGeografica(area);
        } else {
            throw new Exception("Erro no registo de área geográfica.");
        }
    }
}
