/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class PedidoServico {
    private Horario horario;
    private SubPedido subPedido;
    private String desc;
    private int tempo; // int ??? minutos por exemplo
    
    public PedidoServico(Horario horario, String desc, int tempo) {
        this.horario = horario;
        this.desc = desc;
        this.tempo = tempo;
    }
    
    public String getDesc() {
        return desc;
    }
    
}
