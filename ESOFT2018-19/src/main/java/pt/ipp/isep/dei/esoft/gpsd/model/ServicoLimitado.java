/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author diogomagalhaes
 */
public class ServicoLimitado implements Servico {
    private String m_strId;
    private String m_strDescricaoBreve;
    private String m_strDescricaoCompleta;
    private double m_dCustoHora;
    private Categoria m_oCategoria;
    private String limTempo;
    
    public ServicoLimitado(String strId, String strDescricaoBreve, String strDescricaoCompleta, double dCustoHora, Categoria oCategoria, String limTempo)
    {
        if ( (strId == null) || (strDescricaoBreve == null) || (strDescricaoCompleta == null) ||
                (dCustoHora < 0) || (oCategoria == null) ||
                (strId.isEmpty())|| (strDescricaoBreve.isEmpty()) || (strDescricaoCompleta.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        this.m_strId = strId;
        this.m_strDescricaoBreve = strDescricaoBreve;
        this.m_strDescricaoCompleta = strDescricaoCompleta;
        this.m_dCustoHora = dCustoHora;
        m_oCategoria = oCategoria;
        this.limTempo = limTempo;
    }
    
    @Override
    public Categoria getCategoria() {
        return m_oCategoria;
    }
}
