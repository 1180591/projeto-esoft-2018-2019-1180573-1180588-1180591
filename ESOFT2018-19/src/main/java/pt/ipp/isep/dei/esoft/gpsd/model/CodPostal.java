/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class CodPostal {
    private int codPostal;
    private String localidade;
    
    public CodPostal(int codPostal, String localidade) {
        this.codPostal = codPostal;
        this.localidade = localidade;
    }
    
    @Override
    public String toString() {
        return String.format("%d-%d %s", codPostal/1000, codPostal%1000, localidade);
    }
}
