/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class AreaGeografica {
    private String nomeArea;
    private double custoDesloc;
    private double raio;
    private CodPostal codBase;
    
    public AreaGeografica(String nomeArea, double custoDesloc, double raio, CodPostal codBase) {
        this.nomeArea = nomeArea;
        this.custoDesloc = custoDesloc;
        this.raio = raio;
        this.codBase = codBase;
    }
    
    public double getCusto() {
        return custoDesloc;
    }
    
    public CodPostal getCodPostal() {
        return codBase;
    }
}
