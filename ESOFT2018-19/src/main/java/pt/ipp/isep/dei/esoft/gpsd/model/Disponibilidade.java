/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class Disponibilidade {
    private Horario horaInicial;
    private Horario horaFinal;
    private String diaSemana;
    
    public Disponibilidade(String diaSemana, Horario horaInicial, Horario horaFinal) {
        this.horaInicial = horaInicial;
        this.horaFinal = horaFinal;
        this.diaSemana = diaSemana;
    }
}
