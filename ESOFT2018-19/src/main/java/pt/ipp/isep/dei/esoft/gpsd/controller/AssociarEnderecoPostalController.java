/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.gpsd.model.RegistoEnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.Cliente;
/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class AssociarEnderecoPostalController {
    
    public RegistoEnderecoPostal getRegistoEnderecos(Cliente cliente) {
        return new RegistoEnderecoPostal(cliente);
    }
}
