/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class RegistoEnderecoPostal {
    private Cliente cliente;
    
    public EnderecoPostal newEnderecoPostal(String local, CodPostal codPostal, String localidade) {
        return new EnderecoPostal(local, codPostal, localidade);
    }
    
    public boolean registaEnderecoPostal(EnderecoPostal end) {
        return true;
    }
    
    public boolean validaEnderecoPostal(EnderecoPostal end) {
        if (!end.getCodPostal().equals(null) && !end.getLocal().isEmpty() && !end.getLocalidade().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
    
    public void addEnderecoPostal(EnderecoPostal end) {
        cliente.getEnderecos().add(end);
    }
    
    public RegistoEnderecoPostal(Cliente cliente) {
        this.cliente = cliente;
    }
}
