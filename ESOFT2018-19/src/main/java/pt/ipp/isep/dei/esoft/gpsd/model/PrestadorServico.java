/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class PrestadorServico {
    private int numMecanografico;
    private String nAbrev;
    private String nComp;
    private String email;
    private ArrayList<Categoria> categoriasAptas;
    private ArrayList<AreaGeografica> areasAptas;
    private ArrayList<Disponibilidade> disponibilidades;
    
    public PrestadorServico(int numMecanografico, String nAbrev, String nComp, String email, ArrayList<Categoria> categoriasAptas, ArrayList<AreaGeografica> areasAptas, ArrayList<Disponibilidade> disponibilidades) {
        this.numMecanografico = numMecanografico;
        this.nAbrev = nAbrev;
        this.nComp = nComp;
        this.email = email;
        this.categoriasAptas = categoriasAptas;
        this.areasAptas = areasAptas;
        this.disponibilidades = disponibilidades;
    }
    
    public void addCategoria(Categoria cat) {
        this.categoriasAptas.add(cat);
    }
    
    public void addAreaGeografica(AreaGeografica area) {
        this.areasAptas.add(area);
    }
    
    public ArrayList<Disponibilidade> getDisponibilidades() {
        return this.disponibilidades;
    }
}
