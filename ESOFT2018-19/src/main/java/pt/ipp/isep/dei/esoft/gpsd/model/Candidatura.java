package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class Candidatura {
    private String nome;
    private String nif;
    private String contacto;
    private String email;
    
    public Candidatura(String nome, String nif, String contacto, String email) {
        if ( (nome == null) || (nif == null) || contacto == null || email == null ||
                (nome.isEmpty())|| (nif.isEmpty()) || contacto.isEmpty() || email.isEmpty())
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.nome = nome;
        this.nif = nif;
        this.contacto = contacto;
        this.email = email;
    }
    
    @Override
    public String toString() {
        return String.format("%s - %s - %s - %s", nome, nif, contacto, email);
    }
    
    public String getNome() {
        return nome;
    }
    
    public String getEmail() {
        return email;
    }
}
