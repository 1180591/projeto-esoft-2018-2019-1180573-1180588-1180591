package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.awt.Image;
import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;
import pt.ipp.isep.dei.esoft.gpsd.model.Categoria;
import pt.ipp.isep.dei.esoft.gpsd.model.Candidatura;
import pt.ipp.isep.dei.esoft.gpsd.model.CodPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.HabilitacaoA;
import pt.ipp.isep.dei.esoft.gpsd.model.HabilitacaoP;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class SubmitCandidaturaController {
    private Empresa m_oEmpresa;
    List<Candidatura> m_lstCandidaturas;
    
    public List<Categoria> getCategorias() {
        return this.m_oEmpresa.getCategorias();
    }
    
    public Candidatura newCandidatura(String nome, String nif, String contacto, String email) throws Exception {
        try {
                return new Candidatura(nome, nif, contacto, email);
        } catch(Exception e) {
            throw e;
        }
    }
    
    public Categoria newCategoria(String codigo, String desc) {
        List<Categoria> list = getCategorias();
        Categoria correta;
        for (Categoria cat:list) {
            if (cat.getCodigo().equals(codigo)) {
                return cat;
            }
        }
        return null;
    }
    
    public EnderecoPostal newEndereco(String local, CodPostal codPostal, String localidade) {
        return new EnderecoPostal(local, codPostal, localidade);
    }
    
    public HabilitacaoA newHabilitacaoA(Image comprovativo) {
        return new HabilitacaoA(comprovativo);
    }
    
    public HabilitacaoP newHabilitacaoP(Image comprovativo) {
        return new HabilitacaoP(comprovativo);
    }
    
    public boolean validaCandidatura(Candidatura candidatura) {
        //Ainda não sabemos o critério de validação
        return true;
    }
    
    public void addCandidatura(Candidatura candidatura) {
        m_lstCandidaturas.add(candidatura);
    }
    
    public void registaCandidatura(Candidatura candidatura) throws Exception{
        if (validaCandidatura(candidatura)) {
            addCandidatura(candidatura);
        } else {
            throw new Exception("Erro no registo de candidatura.");
        }
    }
}
