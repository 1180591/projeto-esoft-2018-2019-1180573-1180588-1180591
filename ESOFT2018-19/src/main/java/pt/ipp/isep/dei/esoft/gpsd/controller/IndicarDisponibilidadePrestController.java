/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.ArrayList;
import pt.ipp.isep.dei.esoft.gpsd.model.*;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class IndicarDisponibilidadePrestController {
    public ArrayList<Disponibilidade> getListaDisponibilidade(PrestadorServico prest) {
        return prest.getDisponibilidades();
    }
    
    public boolean registaDisponibilidade(Disponibilidade disp) {
        return true;
    }
}
