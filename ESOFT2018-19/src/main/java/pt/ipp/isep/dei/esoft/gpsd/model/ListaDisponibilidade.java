/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class ListaDisponibilidade {
    private PrestadorServico prestador;
    
    public Disponibilidade novaDisponibilidade(String diaSemana, Horario horaInicial, Horario horaFinal) {
        return new Disponibilidade(diaSemana, horaInicial, horaFinal);
    }
    
    public boolean registarDisponibilidade(Disponibilidade disp) {
        return true;
    }
    
    public void addDisponibilidade(Disponibilidade disp) {
        prestador.getDisponibilidades().add(disp);
    }
    
    public boolean validaDisponibilidade(Disponibilidade disp) {
        return true;
    }
}
