/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.List;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class RegistoAreas {
    private Empresa empresa;
    
    public RegistoAreas(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public List<AreaGeografica> getAreasGeograficas(Empresa empresa) {
        return empresa.getAreasGeograficas();
    }
    
    public AreaGeografica getAreaGeograficaByCodBase(CodPostal codBase) {
        for (AreaGeografica a : empresa.getAreasGeograficas()) {
            if (a.getCodPostal().equals(codBase)) {
                return a;
            }
        }
        return null;
    }
}   
