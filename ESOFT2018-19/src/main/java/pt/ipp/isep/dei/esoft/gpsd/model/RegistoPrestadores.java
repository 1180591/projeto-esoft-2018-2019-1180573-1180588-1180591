/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author José Magalhães | Duarte Santos | Bruno Ribeiro
 */
public class RegistoPrestadores {
    private Empresa empresa;
    private List<PrestadorServico> lstPrestadores;
    
    public RegistoPrestadores(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public PrestadorServico newPrestadorServico(int numMecanografico, String nComp, String nAbrev, String email, ArrayList<AreaGeografica> areasAptas, ArrayList<Categoria> categoriasAptas, ArrayList<Disponibilidade> disponibilidades) {
        return new PrestadorServico(numMecanografico, nComp, nAbrev, email, categoriasAptas, areasAptas, disponibilidades);
    }
    
    public boolean registaPrestadorServico(PrestadorServico prestador) {
        return true;
    }
    
    public void getPrestadores(){
        
    }
    
}
